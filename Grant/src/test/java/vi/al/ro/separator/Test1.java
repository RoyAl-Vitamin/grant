package vi.al.ro.separator;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vi.al.ro.separation.Main;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Запускатель для разделения на тестовое и обучающее множество
 */
public class Test1 {

    private static final Logger logger = LoggerFactory.getLogger(Test1.class);

    private static final String originPath = "/home/alex/Downloads/Izlom/lgSet";
    private static final String testPath = "/home/alex/Downloads/Izlom/test";
    private static final String trainingPath = "/home/alex/Downloads/Izlom/traning";
    private static final String[] args = {originPath, testPath, trainingPath};

    @Test
    public void starter() {
        int countTest, countTraining, countAll;

        try {
            cleanDir(Paths.get(testPath));
            cleanDir(Paths.get(trainingPath));
        } catch (IOException e) {
            logger.error("ERROR: ", e);
        }
        Main.main(args);

        countAll = calcFileCount(new File(originPath));
        countTest = calcFileCount(new File(testPath));
        countTraining = calcFileCount(new File(trainingPath));

        Assert.assertEquals(countTest + countTraining, countAll);
    }

    private void cleanDir(Path path) throws IOException {
        if (path.toFile().exists()) {
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
                    Files.delete(file); // this will work because it's always a File
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir); //this will work because Files in the directory are already deleted
                    return FileVisitResult.CONTINUE;
                }
            });
        }

        Files.createDirectories(path);
    }

    /**
     * Считает кол-во файлов в директории, не являющиеся папками
     * @param file
     * @return
     */
    private int calcFileCount(File file) {
        int count = 0;
        if (file == null) { return count; }
        File[] list = file.listFiles();
        if (list == null) { return count; }
        for (File tempFile : file.listFiles()) {
            if (tempFile.isDirectory()) {
                count += calcFileCount(tempFile);
                continue;
            }

            count++;
        }
        return count;
    }
}
