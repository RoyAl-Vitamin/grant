package vi.al.ro.shredder;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Запускатор для нарезателя
 */
public class Test1 {

    private static final Logger logger = LoggerFactory.getLogger(Test1.class);

    private static final String originPath = "/home/alex/Downloads/Izlom/test";
    private static final String outputPath = "/home/alex/Downloads/Izlom/testOut";
//    private static final String originPath = "/home/alex/Downloads/Izlom/traning";
//    private static final String outputPath = "/home/alex/Downloads/Izlom/traningOut";
    private static final String[] args = {originPath, outputPath};

    @Test
    public void starter() {

        Main.main(args);
        // TODO Реализовать рекурсивный подсчёт файлов не являющихся папками
        int count = 1;
        int countAll = 1;
        Assert.assertEquals(count, countAll);
    }
}
