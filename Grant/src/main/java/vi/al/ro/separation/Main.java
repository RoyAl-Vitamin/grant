package vi.al.ro.separation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.Random;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    // Тестовая выборка содержит 20%
    // Обучающая - 80%
    // При сравнительно малом кол-ве фотографий может реальный результат распределения быть меньше 20%
    // TODO Переписать механизм распределения
    private static final float TEST_VALUE = 0.2f;

    private static Random r = new Random();

    public static void main(String... args) {
        if (args.length != 3) return;

        File originDir = new File(args[0]);
        File testDir = new File(args[1]);
        File trainingDir = new File(args[2]);

        logger.info("originDir = [{}]", originDir.getAbsolutePath());
        logger.info("testDir = [{}]", testDir.getAbsolutePath());
        logger.info("trainingDir = [{}]", trainingDir.getAbsolutePath());

        try {
            separate(originDir, testDir, trainingDir);
        } catch (IOException e) {
            logger.error("ERROR: ", e);
        }
    }

    private static void separate(File originDir, File testDir, File trainingDir) throws IOException {
        for (File file : Objects.requireNonNull(originDir.listFiles())) {
            if (file.isDirectory()) {

                File outputTestDir = new File(testDir, file.getName());
                try {
                    Files.createDirectories(outputTestDir.toPath());
                } catch (IOException e) {
                    logger.error("IOException: ", e);
                }

                File outputTrainingDir = new File(trainingDir, file.getName());
                try {
                    Files.createDirectories(outputTrainingDir.toPath());
                } catch (IOException e) {
                    logger.error("IOException: ", e);
                }

                separate(file, outputTestDir, outputTrainingDir);
                continue;
            }

            Path originFilePath = file.toPath();
            Path destinationFilePath = r.nextFloat() < TEST_VALUE ?  testDir.toPath() : trainingDir.toPath();
            Files.copy(originFilePath, destinationFilePath.resolve(originFilePath.getFileName()), StandardCopyOption.REPLACE_EXISTING);
        }
    }
}
