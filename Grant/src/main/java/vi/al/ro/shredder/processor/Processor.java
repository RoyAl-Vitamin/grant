package vi.al.ro.shredder.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Processor {

    private static final Logger logger = LoggerFactory.getLogger(Processor.class);

    /**
     * Thread pool
     */
    private ExecutorService threadPool;

    /**
     * accepted extension
     */
    private static final ExtensionsFilter IMAGE_FILTER_AND_DIR = new ExtensionsFilter(new String[] {".png", ".jpg", ".bmp"});

    /**
     * Threads count
     * For AMD RYZEN 1700X
     */
    private final static int N_THREADS = 16;

    public Processor() {
        threadPool = Executors.newFixedThreadPool(N_THREADS);
    }

    public void process(File file, File outputFile) {

        if (file == null) return;

        if (!file.isDirectory()) {
            if (file.canRead()) {
                ProcTask pt = new ProcTask(file, outputFile);
                threadPool.submit(pt);
            }
            return;
        }

        File outputDir = new File(outputFile, file.getName());
        try {
            Files.createDirectories(outputDir.toPath());
        } catch (IOException e) {
            logger.error("IOException: ", e);
        }

        for (File tempFile: file.listFiles(IMAGE_FILTER_AND_DIR)) {
            this.process(tempFile, outputDir);
        }
    }

    /**
     * See <a href="https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html">documentation</a>.
     */
    public void shutdownAndAwaitTermination() {
        threadPool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!threadPool.awaitTermination(60, TimeUnit.SECONDS)) {
                threadPool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!threadPool.awaitTermination(60, TimeUnit.SECONDS)) {
                    logger.error("Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            threadPool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }
}
