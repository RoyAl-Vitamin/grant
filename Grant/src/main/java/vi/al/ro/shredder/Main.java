package vi.al.ro.shredder;

import vi.al.ro.shredder.processor.Processor;

import java.io.File;

public class Main {

    public static void main(String... args) {
        if (args.length < 2) return;

        File rootDir = new File(args[0]);
        File outputDir = new File(args[1]);

        Processor p = new Processor();

        p.process(rootDir, outputDir);

        p.shutdownAndAwaitTermination();
    }
}
