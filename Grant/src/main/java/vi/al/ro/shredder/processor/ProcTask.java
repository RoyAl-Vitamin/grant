package vi.al.ro.shredder.processor;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcTask implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(ProcTask.class);

    /**
     * Width fragment img [px]
     */
    private static final int WIDTH = 256;

    /**
     * Height fragment img [px]
     */
    private static final int HEIGHT = 256;

    /**
     * This is a picture
     */
    private final File file;

    /**
     * This is a output picture
     */
    private final File outputDir;

    public ProcTask(File file, File outputDir) {
        this.file = file;
        this.outputDir = outputDir;
    }

    @Override
    public void run() {
        BufferedImage img = null;
        try {
            img = ImageIO.read(file);
        } catch (IOException e) {
            logger.error("ERROR: ", e);
        }

        if (img == null) { return; }
        if (img.getHeight() <= HEIGHT && img.getWidth() <= WIDTH) { return; }

        int i = 0;
        for (int x = 0; x <= img.getWidth() - WIDTH; x += WIDTH) {
            for (int y = 0; y <= img.getHeight() - HEIGHT; y += HEIGHT) {
                saveFragment(img.getSubimage(x, y, WIDTH, HEIGHT), i++);
            }
        }

        // DOWN SIDE
        final int Y_LINE = img.getHeight() - HEIGHT;
        if (img.getHeight() % HEIGHT != 0) {
            for (int x = 0; x <= img.getWidth() - WIDTH; x += WIDTH) {
                saveFragment(img.getSubimage(x, Y_LINE, WIDTH, HEIGHT), i++);
            }
        }

        // RIGHT SIDE
        final int X_LINE = img.getHeight() - HEIGHT;
        if (img.getWidth() % WIDTH != 0) {
            for (int y = 0; y <= img.getHeight() - HEIGHT; y += HEIGHT) {
                saveFragment(img.getSubimage(X_LINE, y, WIDTH, HEIGHT), i++);
            }
        }

        // DOWN-RIGHT CORNER
        if (img.getWidth() % WIDTH != 0 && img.getHeight() % HEIGHT != 0) {
            saveFragment(img.getSubimage(X_LINE, Y_LINE, WIDTH, HEIGHT), i);
        }

//        if (!this.file.delete()) {
//            logger.error("Произошла ошибка удаления");
//        }
    }

    private void saveFragment(BufferedImage fragmentImg, int i) {
        String fileName =
                file.getName().substring(0, file.getName().lastIndexOf(".")) +
                "_" + i +
                file.getName().substring(file.getName().lastIndexOf("."));

        File newOutputFile = new File(outputDir, fileName);
        try {
            ImageIO.write(fragmentImg, "jpg", newOutputFile);
        } catch (IOException e) {
            logger.error("ERROR: ", e);
        }
    }
}
