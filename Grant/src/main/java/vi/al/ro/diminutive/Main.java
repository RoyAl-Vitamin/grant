package vi.al.ro.diminutive;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Неправильно рисует (копирует) изображения, теряет часть цвета:
 * <a href="https://stackoverflow.com/questions/4386446/issue-using-imageio-write-jpg-file-pink-background">Link 1</a><br>
 * <a href="http://qaru.site/questions/138550/rroblem-using-imageiowrite-jpg-file">Link 2</a>
 */
public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static double scale;

    private static String outputFolder;

    // Уменьшитель картинок:
    // Преобразует картинки, уменьшая их площадь в 2 раза
    // Например уменьшает фотографии с приближением в 1000 до приближения в 500
    public static void main(String... args) {
        if (args.length == 0) { throw new RuntimeException("ArrayIndexOutOfBoundsException: list files is empty!"); }

        String jsonText;
        try {
            jsonText = new String(Files.readAllBytes(Paths.get(args[0])));
        } catch (IOException ioe) {
            logger.error("ERROR!", ioe);
            throw new RuntimeException(ioe);
        }

        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
        FileDescription fileDescription = gson.fromJson(jsonText, FileDescription.class);

        scale = fileDescription.scale;
        outputFolder = fileDescription.outputFolder;

        if (scale <= 0 || outputFolder == null) {
            throw new RuntimeException("Error in start params");
        }
        logger.info("output folder = {}, scale = {}", outputFolder, scale);
        for (String filePath : fileDescription.files) {
            logger.info("filePath = {}", filePath);

            manipulateImage(filePath);
        }
    }

    private static void manipulateImage(String path) {
        String fileName = null; // Имя файла, с которым он будет сохранён
        BufferedImage img = null;
        try {
            File fileTmp = new File(path);
            fileName = fileTmp.getName();
            img = ImageIO.read(fileTmp);
        } catch (IOException ioe) {
            logger.error("ERROR: ", ioe);
            throw new RuntimeException(ioe);
        }

        if (img == null) {
            logger.debug("Image is NULL");
            return;
        }
        int newW = (int) (img.getWidth() * scale);
        int newH = (int) (img.getHeight() * scale);

        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        File newOutputFile = new File(outputFolder, fileName);
        try {
            ImageIO.write(dimg, "png", newOutputFile);
        } catch (IOException e) {
            logger.error("ERROR: Can't save output file", e);
        }

        logger.info("Save file with new size!");
    }
}
