package vi.al.ro.cutter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Обрезает информацию, полученную от микроскопа, с картинки
 */
public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static final int MAX_HEIGHT = 960;

    private static final int NEW_MAX_HEIGHT = 877;

    private static final int MAX_WIDTH = 1280;

    private static final int MIN_HEIGHT = 480;

    private static final int NEW_MIN_HEIGHT = 438;

    private static final int MIN_WIDTH = 640;

    public static void main(String... args) {
        if (args.length < 2) { throw new RuntimeException("ArrayIndexOutOfBoundsException: dir is empty!"); }

        String rootDir = args[0];
        String outDir = args[1];

        File rootFile = new File(rootDir);
        File outFile = new File(outDir);

        logger.info("rootFile = {}", rootFile.getAbsolutePath());
        logger.info("outFile = {}", outFile.getAbsolutePath());

        cutterRecursivePhoto(rootFile, outFile);
    }

    /**
     * Отрезает рекурсивно
     * @param inputFile исходный файл
     * @param outputDir папка, куда нужно сохранить файл
     */
    private static void cutterRecursivePhoto(File inputFile, File outputDir) {
        if (!inputFile.isDirectory()) {

            cutter(inputFile, outputDir);

            return;
        }

        for (File file : inputFile.listFiles()) {
            if (file.isDirectory()) {
                File outputSubDir = new File(outputDir, file.getName());
                try {
                    Files.createDirectories(outputSubDir.toPath());
                } catch (IOException e) {
                    logger.error("IOException: ", e);
                }
                cutterRecursivePhoto(file, outputSubDir);
            } else {
                cutterRecursivePhoto(file, outputDir);
            }
        }
    }

    private static void cutter(File inputFile, File outputDir) {
        BufferedImage bimg = null;
        try {
            bimg = ImageIO.read(inputFile);
        } catch (IOException e) {
            logger.error("ERROR: ", e);
            throw new RuntimeException("Can't read file");
        }

        if (bimg == null) { return; }
        int tempH = 0;
        int tempW = 0;
        if (bimg.getHeight() == MAX_HEIGHT && bimg.getWidth() == MAX_WIDTH) {
            tempH = NEW_MAX_HEIGHT;
            tempW = MAX_WIDTH;
        }

        if (bimg.getHeight() == MIN_HEIGHT && bimg.getWidth() == MIN_WIDTH) {
            tempH = NEW_MIN_HEIGHT;
            tempW = MIN_WIDTH;
        }
        if (tempH == 0 || tempW == 0) {
            throw new RuntimeException("Invalid size!");
        }


        BufferedImage fragmentBImg = bimg.getSubimage(0, 0, tempW, tempH);

        String fileName = inputFile.getName();

        File newOutputFile = new File(outputDir, fileName);
        try {
            ImageIO.write(fragmentBImg, "jpg", newOutputFile);
        } catch (IOException e) {
            logger.error("ERROR: ", e);
        }
        logger.debug("save photo!");
    }
}
